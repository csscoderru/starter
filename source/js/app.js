import Vue from 'vue';

if (NODE_ENV === 'develop') {
  Vue.config.devtools = true;
}

(function () {
  'use strict';
  $('.l-body').addClass('firs42');
})();
